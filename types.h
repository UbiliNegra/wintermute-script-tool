/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   types.h
 * Author: brigada
 *
 * Created on 12 декабря 2017 г., 13:03
 */

#ifndef TYPES_H
#define TYPES_H

#include <map>
#include <vector>
#include <string>

#define SCRIPT_MAGIC   0xDEC0ADDE
#define SCRIPT_VERSION 0x0102


typedef unsigned int DWORD;

enum WMEInstructionArgument {
    WMEIA_NONE,
    WMEIA_INTEGER,
    WMEIA_STRING,
    WMEIA_FLOAT,
    WMEIA_SYMBOL,
    WMEIA_POSITION
};

struct WMEInstructionDef {
    const char *name;
    WMEInstructionArgument argument_type;
};

static const WMEInstructionDef WMEInstructionDefs[] = {
    {"DEF_VAR", WMEIA_SYMBOL}, // 0
    {"DEF_GLOB_VAR", WMEIA_SYMBOL},
    {"RET", WMEIA_NONE},
    {"RET_EVENT", WMEIA_NONE},
    {"CALL", WMEIA_POSITION},
    {"CALL_BY_EXP", WMEIA_NONE}, // 5
    {"EXTERNAL_CALL", WMEIA_SYMBOL},
    {"SCOPE", WMEIA_NONE},
    {"CORRECT_STACK", WMEIA_INTEGER},
    {"CREATE_OBJECT", WMEIA_NONE},
    {"POP_EMPTY", WMEIA_NONE}, // 10
    {"PUSH_VAR", WMEIA_SYMBOL},
    {"PUSH_VAR_REF", WMEIA_SYMBOL},
    {"POP_VAR", WMEIA_SYMBOL},
    {"PUSH_VAR_THIS", WMEIA_NONE},
    {"PUSH_INT", WMEIA_INTEGER}, // 15
    {"PUSH_BOOL", WMEIA_INTEGER},
    {"PUSH_FLOAT", WMEIA_FLOAT},
    {"PUSH_STRING", WMEIA_STRING},
    {"PUSH_NULL", WMEIA_NONE},
    {"PUSH_THIS_FROM_STACK", WMEIA_NONE}, // 20
    {"PUSH_THIS", WMEIA_SYMBOL},
    {"POP_THIS", WMEIA_NONE},
    {"PUSH_BY_EXP", WMEIA_NONE},
    {"POP_BY_EXP", WMEIA_NONE},
    {"JMP", WMEIA_POSITION}, // 25
    {"JMP_FALSE", WMEIA_POSITION},
    {"ADD", WMEIA_NONE},
    {"SUB", WMEIA_NONE},
    {"MUL", WMEIA_NONE},
    {"DIV", WMEIA_NONE}, // 30
    {"MODULO", WMEIA_NONE},
    {"NOT", WMEIA_NONE},
    {"AND", WMEIA_NONE},
    {"OR", WMEIA_NONE},
    {"CMP_EQ", WMEIA_NONE}, // 35
    {"CMP_NE", WMEIA_NONE},
    {"CMP_L", WMEIA_NONE},
    {"CMP_G", WMEIA_NONE},
    {"CMP_LE", WMEIA_NONE},
    {"CMP_GE", WMEIA_NONE}, // 40
    {"CMP_STRICT_EQ", WMEIA_NONE},
    {"CMP_STRICT_NE", WMEIA_NONE},
    {"DBG_LINE", WMEIA_INTEGER},
    {"POP_REG1", WMEIA_NONE},
    {"PUSH_REG1", WMEIA_NONE},
    {"DEF_CONST_VAR", WMEIA_SYMBOL}, // 46
};

static const DWORD WMEInstructionCount = sizeof (WMEInstructionDefs) / sizeof (WMEInstructionDef);

struct WMEOperation {

    WMEOperation() {
    }

    WMEOperation(const WMEOperation &orig)
    : opcode(orig.opcode), ival(orig.ival), sval(orig.sval), fval(orig.fval) {
    }

    WMEOperation& operator=(const WMEOperation &orig) {
        opcode = orig.opcode;
        ival = orig.ival;
        sval = orig.sval;
        fval = orig.fval;
        return *this;
    }

    DWORD opcode;
    DWORD ival;
    std::string sval;
    double fval;
};

typedef std::vector< std::string > WMEStrings;
typedef std::map< DWORD, std::string > WMEMap;
typedef std::map< DWORD, WMEOperation* > WMEBytecode;

struct WMEScriptHeader {
    DWORD magic;
    DWORD version;
    DWORD code_start;
    DWORD func_table;
    DWORD symbol_table;
    DWORD event_table;
    DWORD externals_table;
    DWORD method_table;
};

// external data types

typedef enum {
    TYPE_VOID = 0,
    TYPE_BOOL,
    TYPE_LONG,
    TYPE_BYTE,
    TYPE_STRING,
    TYPE_FLOAT,
    TYPE_DOUBLE,
    TYPE_MEMBUFFER
} WMEExternalType;

// call types

typedef enum {
    CALL_STDCALL = 0,
    CALL_CDECL,
    CALL_THISCALL
} WMECallType;

struct WMEExternalFunction {

    WMEExternalFunction() {
    }

    WMEExternalFunction(const WMEExternalFunction &orig)
    : dll(orig.dll), name(orig.name), call_type(orig.call_type), returns(orig.returns), params(orig.params) {
    }

    WMEExternalFunction& operator=(const WMEExternalFunction &orig) {
        dll = orig.dll;
        name = orig.name;
        call_type = orig.call_type;
        returns = orig.returns;
        params = orig.params;
        return *this;
    }

    std::string dll;
    std::string name;
    WMECallType call_type;
    WMEExternalType returns;
    std::vector<WMEExternalType> params;
};

typedef std::vector<WMEExternalFunction> WMEExternals;

#endif /* TYPES_H */

