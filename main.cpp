/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brigada
 *
 * Created on 9 декабря 2017 г., 13:50
 */

#include <iostream>

#include "types.h"

int Compile(const char *ifn, const char *ofn);
int Decompile(const char *ifn, const char *ofn);

int main(int argc, char **argv) {
    if (argc == 4 && std::string(argv[1]) == "d") {
        return Decompile(argv[2], argv[3]);
    } else if (argc == 4 && std::string(argv[1]) == "c") {
        return Compile(argv[2], argv[3]);
    } else {
        std::cerr << std::endl;
        std::cerr << "Wintermute Engine script tool v1.0" << std::endl;
        std::cerr << "BRIGADA © 2017" << std::endl;
        std::cerr << std::endl;
        std::cerr << "Usage: " << argv[0] << " <command> <in-file> <out-file>" << std::endl;
        std::cerr << std::endl;
        std::cerr << "Commands:" << std::endl;
        std::cerr << "  c: Compile .xml to .script" << std::endl;
        std::cerr << "  d: Decompile .script to .xml" << std::endl;
        std::cerr << std::endl;

        return -1;
    }
}
