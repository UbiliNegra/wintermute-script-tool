/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   support.h
 * Author: brigada
 *
 * Created on 12 декабря 2017 г., 14:47
 */

#ifndef SUPPORT_H
#define SUPPORT_H

#include <ios>
#include <libxml/tree.h>

#include "types.h"

DWORD ReadDWORD(std::istream &stream);
std::string ReadString(std::istream &stream);
double ReadFloat(std::istream &stream);

void WriteDWORD(std::ostream &stream, const DWORD value);
void WriteString(std::ostream &stream, const std::string value);
void WriteFloat(std::ostream &stream, const double value);

std::string xmlGetAttr(const xmlNodePtr node, const char *name);

#endif /* SUPPORT_H */

