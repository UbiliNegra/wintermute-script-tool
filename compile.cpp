/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <fstream>
#include<iostream>
#include <libxml/tree.h>
#include <algorithm>

#include "types.h"
#include "support.h"

int Compile(const char *ifn, const char *ofn) {
    xmlDocPtr doc;
    xmlNodePtr root;

    WMEScriptHeader header;

    header.magic = SCRIPT_MAGIC;
    header.version = SCRIPT_VERSION;

    doc = xmlReadFile(ifn, NULL, 0);
    if (doc == NULL) {
        std::cerr << "xmlReadFile failed" << std::endl;
        return -1;
    }
    root = xmlDocGetRootElement(doc);

    if (xmlStrcmp(root->name, BAD_CAST "WMEScript") != 0) {
        std::cerr << "wrong root element" << std::endl;
        return -1;
    }


    xmlChar * source_attr = xmlGetProp(root, BAD_CAST "source");

    std::string source = std::string(reinterpret_cast<char*> (source_attr));

    xmlFree(source_attr);

    WMEBytecode bc;

    std::map<std::string, DWORD> labels;

    WMEMap functions;
    WMEMap events;
    WMEMap methods;
    WMEExternals externals;
    WMEStrings symbols;

    xmlNodePtr cur = xmlFirstElementChild(root);
    while (cur) {
        if (xmlStrcmp(cur->name, BAD_CAST "Code") == 0) {

            xmlNodePtr bcn = xmlFirstElementChild(cur);
            DWORD ip = 0;
            while (bcn) {

                xmlChar * content = xmlNodeGetContent(bcn);

                if (xmlStrcmp(bcn->name, BAD_CAST "Label") == 0) {

                    labels[reinterpret_cast<char *> (content)] = ip;

                } else if (xmlStrcmp(bcn->name, BAD_CAST "Function") == 0) {

                    functions[ip] = reinterpret_cast<char *> (content);

                } else if (xmlStrcmp(bcn->name, BAD_CAST "Event") == 0) {

                    events[ip] = reinterpret_cast<char *> (content);

                } else if (xmlStrcmp(bcn->name, BAD_CAST "Method") == 0) {

                    methods[ip] = reinterpret_cast<char *> (content);

                } else {

                    WMEOperation * c = new WMEOperation;

                    for (c->opcode = 0; c->opcode < WMEInstructionCount; c->opcode++) {
                        if (xmlStrcmp(bcn->name, BAD_CAST WMEInstructionDefs[c->opcode].name) == 0) {
                            break;
                        }
                    }

                    if (c->opcode == WMEInstructionCount) {
                        std::cerr << "Unknown instruction " << bcn->name << std::endl;
                        return -1;
                    }

                    bc[ip] = c;
                    ip += 4;

                    switch (WMEInstructionDefs[c->opcode].argument_type) {
                        case WMEIA_SYMBOL:
                        {
                            std::string s(reinterpret_cast<char*> (content));
                            for (c->ival = 0; c->ival < symbols.size(); c->ival++) {
                                if (symbols[c->ival] == s) {
                                    break;
                                }
                            }
                            if (c->ival == symbols.size()) {
                                symbols.push_back(s);
                            }
                            ip += sizeof (c->ival);
                            break;
                        }
                        case WMEIA_POSITION:
                        {
                            c->sval = reinterpret_cast<char*> (content);
                            ip += sizeof (DWORD);
                            break;
                        }
                        case WMEIA_INTEGER:
                        {
                            c->ival = std::atoi(reinterpret_cast<char*> (content));
                            ip += sizeof (c->ival);
                            break;
                        }
                        case WMEIA_FLOAT:
                        {
                            c->fval = atof(reinterpret_cast<char*> (content));
                            ip += sizeof (c->fval);
                            break;
                        }
                        case WMEIA_STRING:
                        {
                            c->sval = std::string(reinterpret_cast<char*> (content));
                            ip += c->sval.size() + 1;
                            break;
                        }
                    }
                }

                xmlFree(content);
                bcn = xmlNextElementSibling(bcn);
            }
        } else if (xmlStrcmp(cur->name, BAD_CAST "Externals") == 0) {
            for (xmlNodePtr N_ext = xmlFirstElementChild(cur); N_ext; N_ext = xmlNextElementSibling(N_ext)) {
                if (xmlStrcmp(N_ext->name, BAD_CAST "External")) {
                    std::cerr << "Unexpected element: got '" << N_ext->name << "', remain External" << std::endl;
                    continue;
                }
                WMEExternalFunction item;
                for (xmlNodePtr N_content = xmlFirstElementChild(N_ext); N_content; N_content = xmlNextElementSibling(N_content)) {
                    xmlChar * content = xmlNodeGetContent(N_content);
                    if (xmlStrcmp(N_content->name, BAD_CAST "DLL") == 0) {
                        item.dll = std::string((char *) content);
                    } else if (xmlStrcmp(N_content->name, BAD_CAST "Name") == 0) {
                        item.name = std::string((char *) content);
                    } else if (xmlStrcmp(N_content->name, BAD_CAST "CallType") == 0) {
                        item.call_type = (WMECallType) std::atoi((char *) content);
                    } else if (xmlStrcmp(N_content->name, BAD_CAST "Returns") == 0) {
                        item.returns = (WMEExternalType) std::atoi((char *) content);
                    } else if (xmlStrcmp(N_content->name, BAD_CAST "Param") == 0) {
                        item.params.push_back((WMEExternalType) std::atoi((char *) content));
                    } else {
                        std::cerr << "Uknown element " << N_content->name << std::endl;
                    }
                    xmlFree(content);
                }
                externals.push_back(item);
            }
        } else {
            std::cerr << "Unexpected tag: " << cur->name << std::endl;
        }
        cur = xmlNextElementSibling(cur);
    }
    xmlFreeDoc(doc);

    //    std::cout << "Writing output"<<std::endl;
    std::ofstream ofs(ofn);
    if (!ofs.is_open()) {
        std::cerr << "Unable to open out stream" << std::endl;
        return -1;
    }

    ofs.write(reinterpret_cast<char*> (&header), sizeof (header));

    WriteString(ofs, source);

    header.code_start = ofs.tellp();

    for (WMEBytecode::iterator it = bc.begin(); it != bc.end(); ++it) {
        //        std::cout<<std::distance(bc.begin(), it)<<" ("<<it->first<<")"<<std::endl;

        WriteDWORD(ofs, it->second->opcode);

        switch (WMEInstructionDefs[it->second->opcode].argument_type) {
            case WMEIA_POSITION:
                if (labels.find(it->second->sval) != labels.end()) {
                    WriteDWORD(ofs, header.code_start + labels[it->second->sval]);
                } else {
                    std::cerr << "Unable to find label '" << it->second->sval << "'" << std::endl;
                }
                break;
            case WMEIA_SYMBOL:
                WriteDWORD(ofs, it->second->ival);
                break;
            case WMEIA_INTEGER:
                WriteDWORD(ofs, it->second->ival);
                break;
            case WMEIA_STRING:
                WriteString(ofs, it->second->sval);
                break;
            case WMEIA_FLOAT:
                WriteFloat(ofs, it->second->fval);
                break;
        }
    }

    header.func_table = ofs.tellp();
    WriteDWORD(ofs, functions.size());
    for (WMEMap::reverse_iterator it = functions.rbegin(); it != functions.rend(); ++it) {
        WriteDWORD(ofs, header.code_start + it->first);
        WriteString(ofs, it->second);
    }

    header.symbol_table = ofs.tellp();
    WriteDWORD(ofs, symbols.size());
    for (WMEStrings::size_type i = 0; i < symbols.size(); ++i) {
        WriteDWORD(ofs, i);
        WriteString(ofs, symbols[i]);
    }

    header.event_table = ofs.tellp();
    WriteDWORD(ofs, events.size());
    for (WMEMap::reverse_iterator it = events.rbegin(); it != events.rend(); ++it) {
        WriteDWORD(ofs, header.code_start + it->first);
        WriteString(ofs, it->second);
    }

    header.externals_table = ofs.tellp();
    WriteDWORD(ofs, externals.size());
    for (DWORD i = 0; i < externals.size(); ++i) {
        WriteString(ofs, externals[i].dll);
        WriteString(ofs, externals[i].name);
        WriteDWORD(ofs, externals[i].call_type);
        WriteDWORD(ofs, externals[i].returns);
        WriteDWORD(ofs, externals[i].params.size());
        for (DWORD p = 0; p < externals[i].params.size(); ++p) {
            WriteDWORD(ofs, externals[i].params[p]);
        }
    }

    header.method_table = ofs.tellp();
    WriteDWORD(ofs, methods.size());
    for (WMEMap::reverse_iterator it = methods.rbegin(); it != methods.rend(); ++it) {
        WriteDWORD(ofs, header.code_start + it->first);
        WriteString(ofs, it->second);
    }

    ofs.seekp(0);
    ofs.write(reinterpret_cast<char*> (&header), sizeof (header));
    ofs.close();
    return 0;
}
