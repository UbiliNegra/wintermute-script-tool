/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <fstream>
#include "support.h"

void WriteDWORD(std::ostream &stream, const DWORD value) {
    stream.write(reinterpret_cast<const char*>(&value), sizeof(value));
}
void WriteString(std::ostream &stream, const std::string value) {
    stream.write(value.data(), value.size() + 1);
}

void WriteFloat(std::ostream &stream, const double value) {
    stream.write(reinterpret_cast<const char*>(&value), sizeof(value));
}

std::string ReadString(std::istream &stream) {
    std::string result;
    char c;
    while ((c = stream.get()) != 0) {
        result.push_back(c);
    }
    return result;
}

double ReadFloat(std::istream &stream) {
    double result;
    stream.read(reinterpret_cast<char *> (&result), sizeof (result));
    return result;
}

DWORD ReadDWORD(std::istream &stream) {
    DWORD result;
    stream.read(reinterpret_cast<char *> (&result), sizeof (DWORD));
    return result;
}

