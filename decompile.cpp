/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <fstream>
#include <iostream>
#include <algorithm>
#include <libxml/xmlwriter.h>

#include "support.h"

int Decompile(const char *ifn, const char *ofn) {
    std::ifstream ifs(ifn);
    if (!ifs.is_open()) {
        std::cerr << "Unable to open input stream" << std::endl;
        return -1;
    }

    WMEScriptHeader header;
    ifs.read(reinterpret_cast<char *> (&header), sizeof (WMEScriptHeader));

    if (header.magic != SCRIPT_MAGIC) {
        std::cerr << "Wrong magic" << std::endl;
        return -2;
    }

    if (header.version != SCRIPT_VERSION) {
        std::cerr << "Wrong version" << std::endl;
        return -2;
    }

    if (header.code_start >= header.func_table ||
            header.func_table >= header.symbol_table ||
            header.symbol_table >= header.event_table ||
            header.event_table >= header.externals_table ||
            header.externals_table >= header.method_table) {
        std::cerr << "Wrong offset : " << ifn << std::endl;

        std::cout << "bytecode  : " << header.code_start << std::endl;
        std::cout << "functions : " << header.func_table << std::endl;
        std::cout << "symbols   : " << header.symbol_table << std::endl;
        std::cout << "events    : " << header.event_table << std::endl;
        std::cout << "externals : " << header.externals_table << std::endl;
        std::cout << "methods   : " << header.method_table << std::endl;

        return -1;
    }

    std::string script_source = ReadString(ifs);

    // FUNCTION TABLE
    ifs.seekg(header.func_table, std::ifstream::beg);
    WMEMap functions;
    DWORD func_count = ReadDWORD(ifs);
    for (DWORD i = 0; i < func_count; ++i) {
        DWORD pos = ReadDWORD(ifs);
        std::string name = ReadString(ifs);
        functions[pos] = name;
    }

    // SYMBOL TABLE
    ifs.seekg(header.symbol_table, std::ifstream::beg);
    WMEStrings symbols;
    DWORD symbol_count = ReadDWORD(ifs);
    symbols.resize(symbol_count);
    for (DWORD i = 0; i < symbol_count; ++i) {
        DWORD idx = ReadDWORD(ifs);
        if (idx != i) {
            std::cerr << "Wrong symbol indexes: expected " << i << ", got " << idx << std::endl;
            return -1;
        }
        symbols[idx] = ReadString(ifs);
    }

    // EVENT TABLE
    ifs.seekg(header.event_table, std::ifstream::beg);
    WMEMap events;
    DWORD event_count = ReadDWORD(ifs);
    for (DWORD i = 0; i < event_count; ++i) {
        DWORD pos = ReadDWORD(ifs);
        std::string name = ReadString(ifs);
        events[pos] = name;
    }

    WMEExternals externals;

    // EXTERNAL TABLE
    ifs.seekg(header.externals_table, std::ifstream::beg);
    DWORD external_count = ReadDWORD(ifs);
    for (DWORD i = 0; i < external_count; ++i) {
        WMEExternalFunction item;
        item.dll = ReadString(ifs);
        item.name = ReadString(ifs);
        item.call_type = (WMECallType) ReadDWORD(ifs);
        item.returns = (WMEExternalType) ReadDWORD(ifs);
        DWORD params_count = ReadDWORD(ifs);
        while (params_count--) {
            item.params.push_back((WMEExternalType) ReadDWORD(ifs));
        }
        externals.push_back(item);
    }

    // METHOD TABLE
    ifs.seekg(header.method_table, std::ifstream::beg);
    WMEMap methods;
    DWORD method_count = ReadDWORD(ifs);
    for (DWORD i = 0; i < method_count; ++i) {
        DWORD pos = ReadDWORD(ifs);
        std::string name = ReadString(ifs);
        methods[pos] = name;
    }

    WMEBytecode bc;

    ifs.seekg(header.code_start);

    DWORD ip;
    while ((ip = ifs.tellg()) < header.func_table) {
        WMEOperation *c = new WMEOperation;
        bc[ip] = c;

        c->opcode = ReadDWORD(ifs);
        if (c->opcode < WMEInstructionCount) {
            switch (WMEInstructionDefs[c->opcode].argument_type) {
                case WMEIA_INTEGER:
                case WMEIA_SYMBOL:
                case WMEIA_POSITION:
                    c->ival = ReadDWORD(ifs);
                    break;
                case WMEIA_FLOAT:
                    c->fval = ReadFloat(ifs);
                    break;
                case WMEIA_STRING:
                    c->sval = ReadString(ifs);
                    break;
            }
        } else {
            std::cerr << ip << "Unknown opcode " << c->opcode << std::endl;
            return -1;
        }
    }

    ifs.close();

    xmlTextWriterPtr xwriter;
    xwriter = xmlNewTextWriterFilename(ofn, 0);
    if (xwriter == NULL) {
        std::cerr << "xmlNewTextWriterFilename failed" << std::endl;
        return -1;
    }

    xmlTextWriterSetIndent(xwriter, 1);
    xmlTextWriterSetIndentString(xwriter, BAD_CAST "  ");

    xmlTextWriterStartDocument(xwriter, "1.0", "utf-8", NULL);

    xmlTextWriterStartElement(xwriter, BAD_CAST "WMEScript");
    xmlTextWriterWriteAttribute(xwriter, BAD_CAST "source", BAD_CAST script_source.data());

    xmlTextWriterStartElement(xwriter, BAD_CAST "Code");

    typedef std::map<DWORD, std::vector<DWORD> > WMEDecompileXRef;

    // collectiong all "landing points"
    WMEDecompileXRef xref;

    for (WMEBytecode::iterator it = bc.begin(); it != bc.end(); ++it) {
        WMEOperation * c = it->second;
        if (WMEInstructionDefs[c->opcode].argument_type == WMEIA_POSITION) {
            xref[c->ival].push_back(it->first);
        }
    }

    bool empty_line = false;
    for (WMEBytecode::iterator it = bc.begin(); it != bc.end(); ++it) {                

        if(empty_line) {
            xmlTextWriterWriteString(xwriter, BAD_CAST "\n");
        }
        
        if(functions.find(it->first) != functions.end()){
            if(!empty_line) {
                xmlTextWriterWriteString(xwriter, BAD_CAST "\n");
                empty_line = true;
            }
            xmlTextWriterWriteElement(xwriter, BAD_CAST "Function", BAD_CAST functions[it->first].data());
        }

        if(events.find(it->first) != events.end()){
            if(!empty_line) {
                xmlTextWriterWriteString(xwriter, BAD_CAST "\n");
                empty_line = true;
            }
            xmlTextWriterWriteElement(xwriter, BAD_CAST "Event", BAD_CAST events[it->first].data());
        }

        if(methods.find(it->first) != methods.end()){
            if(!empty_line) {
                xmlTextWriterWriteString(xwriter, BAD_CAST "\n");
                empty_line = true;
            }
            xmlTextWriterWriteElement(xwriter, BAD_CAST "Method", BAD_CAST methods[it->first].data());
        }

        if (xref.find(it->first) != xref.end()) {            
//            if(!empty_line) {
//                xmlTextWriterWriteString(xwriter, BAD_CAST "\n");
//                empty_line = true;
//            }
            xmlTextWriterWriteFormatElement(xwriter, BAD_CAST "Label", "L_%lu", std::distance(xref.begin(), xref.find(it->first)));
        }

        WMEOperation * c = it->second;

        xmlTextWriterStartElement(xwriter, BAD_CAST WMEInstructionDefs[c->opcode].name);
        switch (WMEInstructionDefs[c->opcode].argument_type) {
            case WMEIA_SYMBOL:
                xmlTextWriterWriteFormatString(xwriter, "%s", symbols[c->ival].data());
                break;
            case WMEIA_POSITION:
                xmlTextWriterWriteFormatString(xwriter, "L_%lu", std::distance(xref.begin(), xref.find(c->ival)));
                break;
            case WMEIA_INTEGER:
                xmlTextWriterWriteFormatString(xwriter, "%d", c->ival);
                break;
            case WMEIA_FLOAT:
                xmlTextWriterWriteFormatString(xwriter, "%f", c->fval);
                break;
            case WMEIA_STRING:
                xmlTextWriterWriteFormatString(xwriter, "%s", c->sval.data());
                break;
        }
        xmlTextWriterEndElement(xwriter);
        
        switch(c->opcode) {
            case 2: // ret
            case 3: // ret_event
            case 25: // jmp
                empty_line = true;
                break;
            default:
                empty_line = false;
        }
    }

    xmlTextWriterEndElement(xwriter); // code

//    xmlTextWriterStartElement(xwriter, BAD_CAST "Functions");
//    std::for_each(functions.begin(), functions.end(), [xwriter, &xref](const WMEMap::value_type item) {
//        xmlTextWriterStartElement(xwriter, BAD_CAST "Function");
//        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "name", "%s", item.second.data());
//        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "target", "L_%lu", std::distance(xref.begin(), xref.find(item.first)));
//        //        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "offset", "%lu", std::distance(bc.begin(), bc.find(item.first)));
//        xmlTextWriterEndElement(xwriter);
//    });
//    xmlTextWriterEndElement(xwriter);

//    xmlTextWriterStartElement(xwriter, BAD_CAST "Events");
//    std::for_each(events.begin(), events.end(), [xwriter, &xref](const std::pair<DWORD, std::string> item) {
//        xmlTextWriterStartElement(xwriter, BAD_CAST "Event");
//        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "name", "%s", item.second.data());
//        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "target", "L_%lu", std::distance(xref.begin(), xref.find(item.first)));
//        //        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "offset", "%lu", std::distance(bc.begin(), bc.find(item.first)));
//        xmlTextWriterEndElement(xwriter);
//    });
//    xmlTextWriterEndElement(xwriter);

    xmlTextWriterStartElement(xwriter, BAD_CAST "Externals");
    for (DWORD i = 0; i < externals.size(); ++i) {
        xmlTextWriterStartElement(xwriter, BAD_CAST "External");
        xmlTextWriterWriteElement(xwriter, BAD_CAST "DLL", BAD_CAST externals[i].dll.data());
        xmlTextWriterWriteElement(xwriter, BAD_CAST "Name", BAD_CAST externals[i].name.data());
        xmlTextWriterWriteFormatElement(xwriter, BAD_CAST "CallType", "%u", externals[i].call_type);
        xmlTextWriterWriteFormatElement(xwriter, BAD_CAST "Returns", "%u", externals[i].returns);
        std::for_each(externals[i].params.begin(), externals[i].params.end(), [xwriter](DWORD param) {
            xmlTextWriterWriteFormatElement(xwriter, BAD_CAST "Param", "%u", param);
        });
        xmlTextWriterEndElement(xwriter);
    }
    xmlTextWriterEndElement(xwriter);

//    xmlTextWriterStartElement(xwriter, BAD_CAST "Methods");
//    std::for_each(methods.begin(), methods.end(), [xwriter, &xref](const WMEMap::value_type item) {
//        xmlTextWriterStartElement(xwriter, BAD_CAST "Method");
//        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "name", "%s", item.second.data());
//        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "target", "L_%lu", std::distance(xref.begin(), xref.find(item.first)));
//        //        xmlTextWriterWriteFormatAttribute(xwriter, BAD_CAST "offset", "%lu", std::distance(bc.begin(), bc.find(item.first)));
//        xmlTextWriterEndElement(xwriter);
//    });
//    xmlTextWriterEndElement(xwriter);

    xmlTextWriterEndDocument(xwriter);

    xmlFreeTextWriter(xwriter);

    return 0;
}

